package hu.bme.aut.android.workplaceapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.utils.ColorTemplate
import hu.bme.aut.android.workplaceapp.R
import hu.bme.aut.android.workplaceapp.data.DataManager
import hu.bme.aut.android.workplaceapp.databinding.PaymentTaxesBinding

class PaymentTaxesFragment : Fragment(R.layout.payment_taxes) {

    private lateinit var binding: PaymentTaxesBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = PaymentTaxesBinding.inflate(inflater, container, false)

        drawChart()

        return binding.root
    }

    private fun drawChart() {
        val salary = DataManager.salaries.last()
        var taxTotal = 0.0
        var entries = mutableListOf<PieEntry>()

        binding.chartTaxes.centerText = salary.toString()

        DataManager.taxes.forEach {
            entries.add(PieEntry((salary * it.percentage / 100).toFloat(), it.name))
            taxTotal += it.percentage
        }
        entries.add(PieEntry((salary * (100 - taxTotal) / 100).toFloat(), "Remaining"))

        val dataSet = PieDataSet(entries, "Taxes")
        dataSet.colors = ColorTemplate.MATERIAL_COLORS.toList()

        val data = PieData(dataSet)
        binding.chartTaxes.data = data
        binding.chartTaxes.invalidate()
    }
}
