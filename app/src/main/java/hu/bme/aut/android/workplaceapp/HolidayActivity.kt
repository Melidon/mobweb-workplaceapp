package hu.bme.aut.android.workplaceapp

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.utils.ColorTemplate
import hu.bme.aut.android.workplaceapp.data.DataManager
import hu.bme.aut.android.workplaceapp.databinding.ActivityHolidayBinding
import hu.bme.aut.android.workplaceapp.fragments.DatePickerDialogFragment
import java.util.*

class HolidayActivity : AppCompatActivity(), DatePickerDialogFragment.OnDateSelectedListener {

    private lateinit var binding: ActivityHolidayBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHolidayBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnTakeHoliday.setOnClickListener {
            DatePickerDialogFragment().show(supportFragmentManager, "DATE_TAG")
        }
        loadHolidays()
    }

    private fun loadHolidays() {
        val entries = listOf(
            PieEntry(DataManager.holidays.toFloat(), "Taken"),
            PieEntry(DataManager.remainingHolidays.toFloat(), "Remaining")
        )

        val dataSet = PieDataSet(entries, "Holidays")
        dataSet.colors = ColorTemplate.MATERIAL_COLORS.toList()

        val data = PieData(dataSet)
        binding.chartHoliday.data = data
        binding.chartHoliday.invalidate()

        if (DataManager.remainingHolidays <= 0) {
            binding.btnTakeHoliday.isEnabled = false
        }
    }

    override fun onDateSelected(year: Int, month: Int, day: Int) {

        if (DataManager.remainingHolidays <= 0) {
            Toast.makeText(this, "You can no longer take time off.", Toast.LENGTH_LONG).show()
            return
        }

        val now = Calendar.getInstance()
        val selected = Calendar.getInstance()
        selected.set(year, month, day)
        if (selected.before(now)) {
            Toast.makeText(this, "You cannot take leave for an earlier day.", Toast.LENGTH_LONG)
                .show()
            return
        }

        val numHolidays = DataManager.holidays
        DataManager.holidays = numHolidays + 1
        loadHolidays()
    }
}
