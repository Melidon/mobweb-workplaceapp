package hu.bme.aut.android.workplaceapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import hu.bme.aut.android.workplaceapp.adapter.PaymentPagerAdapter
import hu.bme.aut.android.workplaceapp.databinding.ActivityPaymentBinding

class PaymentActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPaymentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPaymentBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.vpPayment.adapter = PaymentPagerAdapter(supportFragmentManager)
    }
}