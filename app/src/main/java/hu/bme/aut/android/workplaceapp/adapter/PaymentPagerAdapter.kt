package hu.bme.aut.android.workplaceapp.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import hu.bme.aut.android.workplaceapp.fragments.MonthlyPaymentFragment
import hu.bme.aut.android.workplaceapp.fragments.PaymentTaxesFragment

class PaymentPagerAdapter(fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): Fragment = when (position) {
        0 -> PaymentTaxesFragment()
        1 -> MonthlyPaymentFragment()
        else -> PaymentTaxesFragment()
    }

    override fun getCount(): Int = NUM_PAGES

    companion object {
        const val NUM_PAGES = 2
    }
}