package hu.bme.aut.android.workplaceapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import hu.bme.aut.android.workplaceapp.R
import hu.bme.aut.android.workplaceapp.data.DataManager
import hu.bme.aut.android.workplaceapp.databinding.PaymentMonthlyBinding

class MonthlyPaymentFragment : Fragment(R.layout.payment_monthly) {

    private lateinit var binding: PaymentMonthlyBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = PaymentMonthlyBinding.inflate(inflater, container, false)

        drawChart()

        return binding.root
    }

    private fun drawChart() {
        val salaries = DataManager.salaries
        val entries = mutableListOf<BarEntry>()

        for (i in salaries.indices) {
            entries.add(BarEntry((i + 1).toFloat(), salaries[i].toFloat()))
        }

        val dataSet = BarDataSet(entries, "Salaries")
        val data = BarData(dataSet)
        binding.chartMonthly.data = data
        binding.chartMonthly.invalidate()
    }
}
