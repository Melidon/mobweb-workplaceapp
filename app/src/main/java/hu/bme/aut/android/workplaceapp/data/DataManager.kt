package hu.bme.aut.android.workplaceapp.data

object DataManager {
    val person = Person(
        "Test User", "testuser@domain.com",
        "1234 Test, Random Street 1.",
        "123456AB",
        "123456789",
        "1234567890",
        "0123456789"
    )

    const val HOLIDAY_MAX_VALUE = 20
    const val HOLIDAY_DEFAULT_VALUE = 15

    var holidays = HOLIDAY_DEFAULT_VALUE
    val remainingHolidays get() = HOLIDAY_MAX_VALUE - holidays

    val taxes = listOf<Tax>(Tax("Tax1", 5.0), Tax("Tax2", 27.0))

    val salaries = arrayOf<Double>(
        700000.0,
        750000.0,
        800000.0,
        850000.0,
        900000.0,
        950000.0,
        1000000.0,
        1050000.0,
        1100000.0,
        1200000.0,
        1150000.0,
        1250000.0
    )
}