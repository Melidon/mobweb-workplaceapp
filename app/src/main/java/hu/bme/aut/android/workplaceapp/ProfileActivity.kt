package hu.bme.aut.android.workplaceapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import hu.bme.aut.android.workplaceapp.adapter.ProfilePagerAdapter
import hu.bme.aut.android.workplaceapp.databinding.ActivityProfileBinding

class ProfileActivity : AppCompatActivity() {

    private lateinit var binding: ActivityProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.vpProfile.adapter = ProfilePagerAdapter(supportFragmentManager)
    }
}