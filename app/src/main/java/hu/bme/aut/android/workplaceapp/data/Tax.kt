package hu.bme.aut.android.workplaceapp.data

data class Tax(val name: String, val percentage: Double)